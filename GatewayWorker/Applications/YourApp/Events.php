<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */

//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;

/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    //当客户端连接时触发
    public static function onConnect($client_id)
    {
        Gateway::sendToClient($client_id, json_encode([
            'type' => 'login',
            'client_id' => $client_id
        ]));
    }

    /**
     * 有消息时
     * @param int $client_id
     * @param mixed $message
     */
    public static function onMessage($client_id, $message)
    {
        // 客户端传递的是json数据
        $message_data = json_decode($message, true);
        // 如果没有$_SESSION['fid']说明客户端没有登录
        if(!isset($_SESSION['fid']))
        {
            // 消息类型不是登录视为非法请求，关闭连接
            if($message_data['type'] !== 'login')
            {
                return Gateway::closeClient($client_id);
            }
            // 设置session，标记该客户端已经登录
            $_SESSION['fid'] = $message_data['fid'];
        }
        // 根据类型执行不同的业务
        switch ($message_data['type']) {
            case 'login':
                Gateway::bindUid($client_id, $message_data['fid']);
                Gateway::sendToClient($client_id, json_encode([
                    'type' => 'bild',
                    'client_id' => $client_id,
                    'fid' => $message_data['fid']
                ]));

                // 上线告诉所有人
//                Gateway::sendToAll(json_encode([
//                    'type' => 'online',
//                    'client_id' => $client_id,
//                    'content' => '用户'.$message_data['fid'].'上线了',
//                    'fid' => $message_data['fid']
//                ]));
                echo "\n";
                break;
        }
    }

    /**
     * 当客户端断开连接时
     * @param integer $client_id 客户端id
     */
    public static function onClose($client_id)
    {
        // 当有人退出的时候通知所有人有人退出了
//        $fid = $_SESSION['fid'];
//        $new_message = array('type' =>'logout', 'fid' => $fid,'time' => date('Y-m-d H:i:s'),'content'=>'聊天对象已离线');
//        Gateway::sendToAll(json_encode($new_message));
        echo "\n";
    }
}
