#!/usr/bin/env php
<?php

use think\App;

define('APP_PATH', __DIR__ . '/app/');
// 加载框架引导文件
require __DIR__ . '/../vendor/autoload.php';

// 执行HTTP应用并响应
$http = (new  App())->http;
$response = $http->name('socket')->run();
$response->send();
$http->end($response);

