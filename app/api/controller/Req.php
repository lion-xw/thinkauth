<?php


namespace app\api\controller;


use app\BaseController;
use think\App;
use think\facade\Db;

class Req extends BaseController
{

    public function initialize()
    {
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Headers: Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With, authKey, Accept, Origin, tokene');
        header('Access-Control-Allow-Credentials: true');
    }
    // 获取图片
    public function getImage()
    {
        $type = input('type');
        $site = input('site');
        $data = Db::name('banner')->where(['site'=>$site,'type'=>$type])->field('img,url')->order('sort asc')->select()->toArray();
        if ( !empty($data)){
            foreach ( $data as $k=>&$v){
                if ( !empty($v['img'])){
                    $v['img'] = get_image_url($v['img']);
                }
            }
        }
        return table($data,'','请求成功',200);
    }

    // 获取按钮
    public function getButton()
    {
        $type = input('type');
        $site = input('site');
        $data = Db::name('button')->where(['site'=>$site,'type'=>$type,'status'=>1])->field('title,subtitle,img,url')->order('sort asc')->select()->toArray();
        if ( !empty($data)){
            foreach ( $data as $k=>&$v){
                if ( !empty($v['img'])){
                    $v['img'] = get_image_url($v['img']);
                }
            }
        }
        return table($data,'','请求成功',200);
    }

    // 下载链接
    public function getDownUrl()
    {
        $data = Db::name('down')->where('type',input('type',1))->field('type,down_url')->find();
        return table($data,'','请求成功',200);
    }

    // 获取公告
    public function getNotice()
    {
        $data = Db::name('notice')->column('content');
        return table($data,'','',200);
    }

    public function ping()
    {
        $ip = "server.aimaster.top";
        $ping_cmd = "ping -c 3 -w 5 " . $ip;
        $time = system($ping_cmd);
        halt($time);
        return table($time);
    }

}