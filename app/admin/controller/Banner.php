<?php


namespace app\admin\controller;


use think\facade\Db;

class Banner extends Base
{
    public function lst()
    {
        return view();
    }

    // 列表数据
    public function listData()
    {
        $data  =  Db::name('banner')->page(input('page/d'),input('limit/d'))->order('sort asc,type asc')->select();
        $count =  Db::name('banner')->count();
        return table($data,$count);
    }

    // 添加页面
    public function addView()
    {
        return view('add');
    }

    // 添加提交
    public function addPost()
    {
        $param = input('post.');
        $data['sort'] = $param['sort'];
        $data['type'] = $param['type'];
        $data['url']  = $param['url'];
        $data['img']  = $param['img'];
        $data['site'] = $param['site'];
        Db::name('banner')->insert($data);
        return suc('添加成功');
    }

    // 修改页面
    public function editView($id)
    {
        $data = Db::name('banner')->find($id);
        return view('edit',['data'=>$data]);
    }

    // 修改提交
    public function editPost()
    {
        $param = input('post.');
        $data['id'] = $param['id'];
        $data['sort'] = $param['sort'];
        $data['type'] = $param['type'];
        $data['url']  = $param['url'];
        $data['img']  = $param['img'];
        $data['site'] = $param['site'];
        $res = Db::name('banner')->update($data);
        if ( $res !== false) {
            return  suc('编辑成功');
        } else {
            return  err('编辑失败');
        }
    }

    # 删除
    public function del()
    {
        $id = input('id');
        $banner = Db::name('banner')->find($id);
        $rt = Db::name('banner')->delete($id);
        if ($rt){
            // 删除图片
            $url = public_path().$banner['img'];
            @unlink($url);
            return suc('删除成功');
        }else{
            return err('删除失败');
        }
    }

    // 图片上传
    public function upload(){
        // 获取表单上传文件
        $file = request()->file('file');
        try {
            $msg = validate(
                [
                    'file' => [
                        // 限制文件大小(单位b)，这里限制为4M
                        'fileSize' => 1 * 1024 * 1024,
                        // 限制文件后缀，多个后缀以英文逗号分割
                        'fileExt'  => 'gif,png'
                    ]
                ],
                [
                    'file.fileSize' => '文件太大,最大限制1M',
                    'file.fileExt' => '不支持的文件后缀',
                ]
            )->check(['file' => $file]);
            $savename = \think\facade\Filesystem::disk('public')->putFile( 'banner', $file);
            $info = str_replace("\\","/",$savename);
            // 读取磁盘配置名为public下的url配置项
            $url     = \think\Facade\Filesystem::getDiskConfig('public', 'url');
            $data['code'] = 1;
            $data['msg'] = '上传成功';
            $data['src']=$url.'/'.$info;
            return json($data);
        } catch (\think\exception\ValidateException $e) {
            return err($e->getMessage());
        }
    }

}