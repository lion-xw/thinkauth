<?php


namespace app\admin\controller;


use think\facade\Db;

class Button extends Base
{
    public function lst()
    {
        return view();
    }

    // 列表数据
    public function listData()
    {
        $data  =  Db::name('button')->page(input('page/d'),input('limit/d'))->order('sort asc')->select();
        $count =  Db::name('button')->count();
        return table($data,$count);
    }

    // 添加页面
    public function addView()
    {
        return view('add');
    }

    // 添加提交
    public function addPost()
    {
        $param = input('post.');
        $data['title'] = $param['title'];
        $data['site'] = $param['site'];
        $data['subtitle'] = $param['subtitle'];
        $data['sort'] = $param['sort'];
        $data['type'] = $param['type'];
        $data['url']  = $param['url'];
        $data['img']  = $param['img'];
        $data['status'] = isset($param['status']) ? 1 : 0;
        $data['create_time'] = date('Y-m-d H:i:s');
        Db::name('button')->insert($data);
        return suc('添加成功');
    }

    // 修改页面
    public function editView($id)
    {
        $data = Db::name('button')->find($id);
        return view('edit',['data'=>$data]);
    }

    // 修改提交
    public function editPost()
    {
        $param = input('post.');
        $data['id'] = $param['id'];
        $data['site'] = $param['site'];
        $data['title'] = $param['title'];
        $data['subtitle'] = $param['subtitle'];
        $data['sort'] = $param['sort'];
        $data['type'] = $param['type'];
        $data['url']  = $param['url'];
        $data['img']  = $param['img'];
        $data['status'] = isset($param['status']) ? 1 : 0;
        $data['create_time'] = date('Y-m-d H:i:s');
        $res = Db::name('button')->update($data);
        if ( $res !== false) {
            return  suc('编辑成功');
        } else {
            return  err('编辑失败');
        }
    }

    # 删除
    public function del()
    {
        $id = input('id');
        $button = Db::name('button')->find($id);
        $rt = Db::name('button')->delete($id);
        if ($rt){
            // 删除图片
            $url = public_path().$button['img'];
            @unlink($url);
            return suc('删除成功');
        }else{
            return err('删除失败');
        }
    }

    // 图片上传
    public function upload(){
        // 获取表单上传文件
        $file = request()->file('file');
        try {
            $msg = validate(
                [
                    'file' => [
                        // 限制文件大小(单位b)，这里限制为4M
                        'fileSize' => 1 * 1024 * 1024,
                        // 限制文件后缀，多个后缀以英文逗号分割
                        'fileExt'  => 'gif,png'
                    ]
                ],
                [
                    'file.fileSize' => '文件太大,最大限制1M',
                    'file.fileExt' => '不支持的文件后缀',
                ]
            )->check(['file' => $file]);
            $savename = \think\facade\Filesystem::disk('public')->putFile( 'button', $file);
            $info = str_replace("\\","/",$savename);
            // 读取磁盘配置名为public下的url配置项
            $url     = \think\Facade\Filesystem::getDiskConfig('public', 'url');
            $data['code'] = 1;
            $data['msg'] = '上传成功';
            $data['src']=$url.'/'.$info;
            return json($data);
        } catch (\think\exception\ValidateException $e) {
            return err($e->getMessage());
        }
    }

}