<?php
declare (strict_types = 1);

namespace app\admin\controller;


use GatewayClient\Gateway;
use think\facade\Db;

use think\facade\View;
use think\Validate;

/**
 * 后台客服控制器
 * Class chat_user
 * @package app\chat_user\controller
 */
class Chat extends Base
{
    // 客服列表
    public function lst()
    {
        $group = $this->app->db->name('chat_group')->where('state',0)->field('id,group_name')->select();
        return view('lst',['group'=>$group]);
    }

    // 列表数据
    public function listData()
    {

        $group_id = input('group_id', '');
        $state    = input('state', '');
        $name     = input('name', '');
        $page     = input('page/d');
        $limit    = input('limit/d');
        $where = [];
        if (!empty($group_id)) {
            $where['group_id'] = $group_id;
        }
        if (!empty($state)) {
            $where['state'] = $state;
        }
        if (!empty($name)) {
            $where[] = array('name','like',$name.'%');
        }
        $data = Db::name('chat_user')
            ->order('group_id asc')
            ->where($where)
            ->page($page,$limit)
            ->select()
            ->each(function ($item) {
                $item['group_name'] = Db::name('chat_group')->where('id', $item['group_id'])->value('group_name');
                //$item['is_online'] = Gateway::isUidOnline($item['name']);
                $item['is_online'] = 0;
                return $item;
            });
        $count =  Db::name('chat_user')->where($where)->count();
        return table($data,$count);
    }

    // 添加显示页面
    public function addView()
    {
        $role = Db::name('auth_group')->where('status',1)->field('id,title')->select()->toArray();
        return view('add',['role'=>$role]);
    }

    // 添加数据提交
    public function addPost()
    {
        $group_id = input('group_id');
        $name     = input('name');
        $pw       = input('password');
        $c_pw     = input('check_password');
        $validate = new Validate([
            'name|用户名' => 'require|min:5',
            'pw|密码' => 'require|min:6',
            'c_pw|重复密码' => 'require|min:6'
        ]);

        $data = [
            'name' => $name,
            'pw' => $pw,
            'c_pw' => $c_pw,
        ];
        if (!$validate->check($data)) {
            return err($validate->getError());
        }
        if ($pw <> $c_pw) {
            return err('两次密码不一致');
        }
        $map['name'] = $name;
        $chat_username = Db::name('chat_user')->where($map)->find();
        if ($name == $chat_username['name']) {
            return err('用户名已存在');
        }
        $save['name'] = $name;
        $save['pw']   = md5($pw);
        $save['login_ip'] = '0.0.0.0';
        $save['group_id'] = $group_id;
        $save['add_time'] = date('Y-m-d h:i:s', time());
        $db = Db::name('chat_user')->insert($save);
        if ($db ){
            return suc('添加成功');
        }else{
            return err('添加失败');
        }
    }

    // 修改显示页面
    public function editView($id)
    {
        $data = Db::name('chat_user') ->find($id);
        View::assign('data', $data);
        $role = Db::name('auth_group')->where('status',1)->field('id,title')->select()->toArray();
        return view('edit',['role'=>$role]);
    }

    // 修改数据提交
    public function editPost()
    {
        $group_id = input('group_id');
        $state    = input('state',1);
        $id       = input('id');
        $pw       = input('password');
        $c_pw     = input('check_password');
        $chat_user = Db::name('chat_user') ->where('id', $id) ->find();
        if (!$chat_user) {
            return err('请重试') ;
        }
        if ( !empty($pw) && !empty($c_pw)){
            if ($pw <> $c_pw) {
                return err('两次密码不一致');
            }
            if ($chat_user['pw'] == md5($pw)) {
                return err('新密码与旧密码一致');
            }
            if ($pw) {
                $validate = new Validate([
                    'pw|密码' => 'require|min:6',
                    'c_pw|重复密码' => 'require|min:6',
                ]);
                $data['pw']   = $pw;
                $data['c_pw'] = $c_pw;
                $save['pw']   = md5($pw);
            }
            if (!empty($validate)) {
                if (!$validate->check($data)) {
                    return  err($validate->getError());
                }
            }
        }
        $save['state']    = $state;
        $save['group_id'] = $group_id;
        $save['id']       = $id;
        if (!empty($save)) {
            $db_chat_user = Db::name('chat_user')->update($save);
        }
        if ( $db_chat_user !== false) {
            return suc('修改成功');
        } else {
            return err('未修改');
        }
    }

    //删除客服
    public function del()
    {
        $id = input('id');
        $name = Db::name('chat_user')
            ->where('id', $id)
            ->value('name');
        if ((int)$id !== 1) {
            if ($name !== session('chat_user')) {
                $db = Db::name('chat_user')
                    ->where('id', $id)
                    ->delete();
                if ($db !== false) {
                    return suc('删除成功');
                } else {
                    return err('删除失败');

                }
            } else {
                return err('无法删除当前登录用户');
            }
        } else {
            return err('超级客服无法删除');
        }
    }

    //禁用启用客服
    public function statechat_user()
    {
        $id = input('id');
        $state = input('checked');
        $name = Db::name('chat_user')
            ->where('id', $id)
            ->value('name');
        if ((int)$id !== 1) {
            if ($name !== session('chat_user')) {
                $db = Db::name('chat_user')
                    ->where('id', $id)
                    ->update(['state' => $state]);
                if ($db !== false) {
                    return ['msg' => '成功', 'code' => '200'];
                } else {
                    return ['msg' => '失败'];
                }
            } else {
                return ['msg' => '无法禁用当前登录用户'];
            }
        } else {
            return ['msg' => '超级客服无法禁用'];
        }
    }





}
