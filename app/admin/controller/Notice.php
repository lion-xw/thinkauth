<?php


namespace app\admin\controller;


use think\facade\Db;

class Notice extends Base
{
    public function lst()
    {
        return view();
    }

    // 列表数据
    public function listData()
    {
        $data  =  Db::name('notice')->page(input('page/d'),input('limit/d'))->select();
        $count =  Db::name('notice')->count();
        return table($data,$count);
    }

    // 添加页面
    public function addView()
    {
        return view('add');
    }

    // 添加提交
    public function addPost()
    {
        $param = input('post.');
        $data['title']   = $param['title'];
        $data['content'] = $param['content'];
        $data['add_time']= date('Y-m-d H:i:s');
        Db::name('notice')->insert($data);
        return suc('添加成功');
    }

    // 修改页面
    public function editView($id)
    {
        $data = Db::name('notice')->find($id);
        return view('edit',['data'=>$data]);
    }

    // 修改提交
    public function editPost()
    {
        $param = input('post.');
        $data['id'] = $param['id'];
        $data['title']   = $param['title'];
        $data['content'] = $param['content'];
        $res = Db::name('notice')->update($data);
        if ( $res !== false) {
            return  suc('编辑成功');
        } else {
            return  err('编辑失败');
        }
    }

    # 删除
    public function del()
    {
        $id = input('id');
        $rt = Db::name('notice')->delete($id);
        if ($rt){
            return suc('删除成功');
        }else{
            return err('删除失败');
        }
    }

}