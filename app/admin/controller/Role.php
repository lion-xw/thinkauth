<?php


namespace app\admin\controller;


use think\facade\Db;

/**
 * 角色权限管理控制器
 * Class Role
 * @package app\admin\controller
 */
class Role extends Base
{

    // 角色列表
    public function lst()
    {
        return view('');
    }

    // 角色列表数据
    public function listData()
    {
        $data  =  Db::name('auth_group')->page(input('page/d'),input('limit/d'))->select();
        $count =  Db::name('auth_group')->count();
        return table($data,$count);
    }

    // 添加角色页面
    public function addView()
    {
        return view('add');
    }

    // 添加提交
    public function addPost()
    {
            $auth_group = input('title');
            $status     = input('status');
            $res = Db::name('auth_group')
                ->where('title', $auth_group)
                ->find();
            if (empty($res)) {
                Db::name('auth_group')
                    ->insert(['title' => $auth_group, 'status' => $status]);
                return  suc('添加成功');
             } else {
                return  err('角色名重复');
            }
    }

    // 修改页面
    public function editView($id)
    {
        $data = Db::name('auth_group')->find($id);
        return view('edit',['data'=>$data]);
    }

    // 修改提交
    public function editPost()
    {
        $auth_group = input('title');
        $status     = input('status');
        $id         = input('id');
        $res = Db::name('auth_group')
            ->update(['title' => $auth_group, 'status' => $status,'id'=>$id]);
        if ( $res !== false) {
            return  suc('编辑成功');
        } else {
            return  err('编辑失败');
        }
    }

    // 角色授权页面
    public function authView($id)
    {
        # 处理默认选中状态
        return view('auth',['id'=>$id]);
    }

    // 授权提交
    public function addAuthPost()
    {
        if ($this->request->isAjax()){
            $save = input('post.');
            $rt = Db::name('auth_group')->update($save);
            if ($rt !== false){
                return suc('授权成功');
            }else{
                return  err('授权失败');
            }
        }
    }
    // 菜单树数据处理
    function get_auth_tree($data,$pid=0,$my_role=array())
    {
        $tree = [];
        foreach ( $data as $key => $row) {
            $row['spread'] = true;
            if($row['pid'] == $pid){
                $children = getTree($data,$row['id'],$my_role);
                if ( !empty($children) ){
                    foreach ( $children as $k=>$v){
                        if ( in_array($v['id'],$my_role ) ){
                            $children[$k]['checked'] = true;
                        }
                    }
                }
                $row['children'] = $children;
                $tree[] = $row;
            }
        }
        return $tree;
    }
    // 获取菜单数据
    public function authData($id)
    {
        // 查询角色当权限
        $role_rules = Db::name('auth_group')
            ->where('id',$id)->value('rules');
       if ( !empty($role_rules)){
           $role_rules =  explode(',',$role_rules);
       }
        $data = Db::name('auth_rule')->select()->toArray();
       if (empty($role_rules)){
           $auth = getTree($data);
       }else{
           $auth = $this->get_auth_tree($data,0,$role_rules);
       }
        return table($auth);
    }

    public function del()
    {
        $id = input('id');
        $rt = Db::name('auth_group')->delete($id);
        if ($rt){
            return suc('删除成功');
        }else{
            return err('删除失败');
        }
    }

}