# thinkauth

#### 介绍
后台thinkPHP6.0 UI：layui ，权限认证、权限分配（authtree）、菜单管理、登录

#### 软件架构
thinkphp6.0


#### 安装教程

1.  缺少数据库文件 觉得能用本人QQ:11169028
2.  游客账号登登录之后看不到管理员模块


#### 预览地址

1.  pro.aimaster.top
2.  游客账号 joker
3.  密码 111111

### 部分页面截图
登录页面
![登录页面](https://images.gitee.com/uploads/images/2021/0924/162730_bffa73b3_7395856.png "1.png")
角色权限树
![角色权限树](https://images.gitee.com/uploads/images/2021/0924/162929_8bdecdb8_7395856.png "2.png")
菜单管理
![菜单管理](https://images.gitee.com/uploads/images/2021/0924/163046_1c89093a_7395856.png "3.png")
列表筛选操作
![列表筛选操作](https://images.gitee.com/uploads/images/2021/0924/163152_98bcd493_7395856.png "4.png")
图片上传处理
![图片上传处理](https://images.gitee.com/uploads/images/2021/0924/163305_8567d436_7395856.png "5.png")
